package com.example.aps_bruno_guerra2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SalvaProdutoActivity extends AppCompatActivity {


    private TextView tvTitulo, tvPreco, tvQtdP;
    private Button btnSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salvaproduto);


        tvTitulo = (TextView) findViewById(R.id.tvNomeProd);
        tvPreco = (TextView) findViewById(R.id.tvprecoProd);
        tvQtdP = (TextView) findViewById(R.id.tvQtdProd);
        btnSalvar = (Button) findViewById(R.id.btnSalvarProd);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarProduto();
            }
        });
    }

    private void salvarProduto(){
        Produtos produto = new Produtos();
        produto.setNome_produto( tvTitulo.getText().toString() );
        produto.setPreco_produto( tvPreco.getText().toString() );
        produto.setQuantidade_produto( tvPreco.getText().toString() );
        ProdutosDAO.inserirProduto(produto, this);
        this.finish();
    }
}