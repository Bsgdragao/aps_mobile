package com.example.aps_bruno_guerra2;

import android.content.Context;
import android.database.sqlite.*;


public class BancoDeListas extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "notas";
    private static final int VERSAO_BANCO = 1;

    public BancoDeListas(Context context){
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS listas ( " +
                "  codigo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , " +
                "  data TEXT , " +
                "  nome TEXT  ) " );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}