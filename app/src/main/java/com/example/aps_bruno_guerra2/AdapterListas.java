package com.example.aps_bruno_guerra2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterListas extends BaseAdapter {

    private Context context;
    private List<Listas> lista;
    private LayoutInflater inflater;

    public AdapterListas(Context context, List<Listas> lista){
        this.context = context;
        this.lista = lista;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).getId_lista();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Suporte item;

        if( convertView == null ){
            convertView = inflater.inflate
                    (R.layout.layout_listas, null);
            item = new Suporte();
            item.tvCodigo = (TextView)convertView.findViewById(R.id.tvCodListaLyout);
            item.tvData = (TextView)convertView.findViewById(R.id.tvDataListaLayout);
            item.tvTitulo = (TextView)convertView.findViewById(R.id.tvNomeLista);


            convertView.setTag(item);
        }else {
            item = (Suporte) convertView.getTag();
        }

        Listas listas = lista.get( position );
        item.tvCodigo.setText( String.valueOf( listas.getId_lista() ) );
        item.tvData.setText(  listas.getData()  );
        item.tvTitulo.setText(  listas.getNomeLista() );



        return convertView;
    }

    private class Suporte{
        TextView tvCodigo, tvTitulo, tvData;
    }

}



