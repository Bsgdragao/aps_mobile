package com.example.aps_bruno_guerra2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ListasDAO  {

    public static final void inserir(Listas lista, Context context){
        BancoDeListas banco = new BancoDeListas(context);
        ContentValues valores = new ContentValues();
        //valores.put("codigo", lista.getId_lista());
        valores.put("data", lista.getData() );
        valores.put("nome", lista.getNomeLista() );
        SQLiteDatabase db = banco.getWritableDatabase();
        db.insert("listas", null, valores);
    }

    public static final void excluir(int cod, Context context){
        BancoDeListas banco = new BancoDeListas(context);
        SQLiteDatabase db = banco.getWritableDatabase();
        db.delete("listas", "codigo = "+cod, null);
    }

    public static final List<Listas> listar(Context context){
        List<Listas> listas = new ArrayList<>();
        BancoDeListas banco = new BancoDeListas(context);
        SQLiteDatabase db = banco.getReadableDatabase();
        String sql = "SELECT * FROM listas ORDER BY codigo DESC ";
        Cursor cursor = db.rawQuery(sql, null);
        if ( cursor.getCount() > 0 ){
            cursor.moveToFirst();
            do{
                Listas lista = new Listas();
                lista.setId_lista( cursor.getInt( 0 ) );
                lista.setData( cursor.getString( 1 ) );
                lista.setNomeLista(cursor.getString( 2 ) );
                listas.add( lista );
            }while ( cursor.moveToNext() );
        }
        return listas;
    }

}
