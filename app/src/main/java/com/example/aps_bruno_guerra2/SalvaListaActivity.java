package com.example.aps_bruno_guerra2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SalvaListaActivity extends AppCompatActivity{

        private EditText etData;
        private TextView tvTitulo;
        private Button btnSalvar;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salvalista);

        etData = (EditText) findViewById(R.id.etDataLista2);
        tvTitulo = (TextView) findViewById(R.id.tvNomelista2);
        btnSalvar = (Button) findViewById(R.id.btnSalvaLista);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarLista();
            }
        });
    }

        private void salvarLista(){
        Listas lista = new Listas();
       lista.setData( etData.getText().toString() );
        lista.setNomeLista( tvTitulo.getText().toString() );
        ListasDAO.inserir(lista, this);
        this.finish();
    }
    }