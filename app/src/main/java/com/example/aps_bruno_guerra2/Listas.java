package com.example.aps_bruno_guerra2;

import android.icu.text.DateTimePatternGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Listas {

    private int id_lista;
    private String nomeLista;
    private String data;

    @Override
    public String toString() {
        return this.nomeLista + "\n" + this.nomeLista;
    }

    public int getId_lista() {
        return id_lista;
    }

    public void setId_lista(int id_lista) {
        this.id_lista = id_lista;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNomeLista() {
        return nomeLista;
    }

    public void setNomeLista(String nomeLista) {
        this.nomeLista = nomeLista;
    }
}
