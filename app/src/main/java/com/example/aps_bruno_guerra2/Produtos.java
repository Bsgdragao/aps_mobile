package com.example.aps_bruno_guerra2;

import java.util.List;

public class Produtos {

    private int id_produto;
    private String nome_produto;
    private String preco_produto;
    private String quantidade_produto;


    public int getId_produto() {
        return id_produto;
    }

    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getPreco_produto() {
        return preco_produto;
    }

    public void setPreco_produto(String preco_produto) {
        this.preco_produto = preco_produto;
    }

    public String getQuantidade_produto() {
        return quantidade_produto;
    }

    public void setQuantidade_produto(String quantidade_produto) {
        this.quantidade_produto = quantidade_produto;
    }
}


