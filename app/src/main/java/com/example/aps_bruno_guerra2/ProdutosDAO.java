package com.example.aps_bruno_guerra2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ProdutosDAO {

    public static final void inserirProduto(Produtos produtos, Context context){
        BancoDeProdutos banco = new BancoDeProdutos(context);
        ContentValues valores = new ContentValues();
        valores.put("nome_produto", produtos.getNome_produto());
        valores.put("preco_produto", produtos.getPreco_produto());
        valores.put("quantidade_produto", produtos.getQuantidade_produto());
        SQLiteDatabase db = banco.getWritableDatabase();
        db.insert("produtos", null, valores);
    }

    public static final void excluirProduto(int idProduto, Context context){
        BancoDeListas banco = new BancoDeListas(context);
        SQLiteDatabase db = banco.getWritableDatabase();
        db.delete("produtos", "id_produto = "+idProduto, null);
    }

    public static final List<Produtos> listarProduto(Context context){
        List<Produtos> prods = new ArrayList<>();
        BancoDeProdutos banco = new BancoDeProdutos(context);
        SQLiteDatabase db = banco.getReadableDatabase();
        String sql = "SELECT * FROM produtos ORDER BY id_produto DESC ";
        Cursor cursor = db.rawQuery(sql, null);
        if ( cursor.getCount() > 0 ){
            cursor.moveToFirst();
            do{
                Produtos produtos = new Produtos();
                produtos.setId_produto( cursor.getInt( 0 ) );
                produtos.setNome_produto( cursor.getString( 1 ) );
                produtos.setPreco_produto( cursor.getString( 2 ) );
                produtos.setQuantidade_produto(cursor.getString(3));
                prods.add( produtos );
            }while ( cursor.moveToNext() );
        }
        return prods;
    }

}


