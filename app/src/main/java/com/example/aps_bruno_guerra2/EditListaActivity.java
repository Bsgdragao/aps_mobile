package com.example.aps_bruno_guerra2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class EditListaActivity extends AppCompatActivity{


    protected void onCreate() {
        onCreate();
    }


    ListView lvProdutos;
    List<Produtos> produto;

    AdapterProdutos adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editlista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lvProdutos = (ListView) findViewById(R.id.lv_produtos);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabListaAddProds);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { Intent intent = new Intent((Context) EditListaActivity.this, SalvaProdutoActivity.class);

                startActivity(intent);
            }
        });

        lvProdutos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                final Produtos prodSelecionado = produto.get(position);
                AlertDialog.Builder alerta = new AlertDialog.Builder(EditListaActivity.this);
                alerta.setTitle("Excluir Produto...");
                alerta.setMessage("Confirma a exclusão do produto? " + prodSelecionado.getNome_produto() + "?");
                alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ListasDAO.excluir(prodSelecionado.getId_produto(), EditListaActivity.this);
                        carregarListaProd();


                    }
                });
                alerta.setNeutralButton("Cancelar", null);
                alerta.show();



                return true;
            }
        });




    }


    private void carregarListaProd(){

        produto = ProdutosDAO.listarProduto(this);
        adapter = new AdapterProdutos(this, produto);
        lvProdutos.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarListaProd();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    }
