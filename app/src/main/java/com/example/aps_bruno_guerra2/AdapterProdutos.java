package com.example.aps_bruno_guerra2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterProdutos extends BaseAdapter {

    private Context context;
    private List<Produtos> listaP;
    private LayoutInflater inflater;

    public AdapterProdutos(Context context, List<Produtos> listaP){
        this.context = context;
        this.listaP = listaP;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listaP.size();
    }

    @Override
    public Object getItem(int position) {
        return listaP.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaP.get(position).getId_produto();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Suporte item;

        if( convertView == null ){
            convertView = inflater.inflate(R.layout.layout_produtos, null);
            item = new Suporte();
            item.tvCodProd= (TextView)convertView.findViewById(R.id.tvCodProd);
            item.tvProd= (TextView)convertView.findViewById(R.id.tvProd);
            item.tvPreco = (TextView)convertView.findViewById(R.id.tvPreco);
            item.tvQtd = (TextView)convertView.findViewById(R.id.tvQtd);


            convertView.setTag(item);
        }else {
            item = (Suporte) convertView.getTag();
        }

        Produtos produto = listaP.get(position);
        item.tvCodProd.setText( String.valueOf( produto.getId_produto() ) );
        item.tvProd.setText(  produto.getNome_produto()  );
        item.tvPreco.setText(produto.getPreco_produto());
        item.tvQtd.setText(produto.getQuantidade_produto());





        return convertView;
    }

    private class Suporte{
        TextView tvCodProd, tvProd, tvPreco, tvQtd;
    }

}

