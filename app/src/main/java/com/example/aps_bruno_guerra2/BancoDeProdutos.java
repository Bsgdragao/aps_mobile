package com.example.aps_bruno_guerra2;

import android.content.Context;
import android.database.sqlite.*;


public class BancoDeProdutos extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "listaprodutos";
    private static final int VERSAO_BANCO = 1;

    public BancoDeProdutos(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS produtos ( " +
                "  id_produto INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , " +
                "  nome_produto TEXT , " +
                "  preco_produto TEXT , " +
                "  quantidade_produto TEXT  ) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}


